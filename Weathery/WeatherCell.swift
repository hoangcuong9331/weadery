//
//  WeatherCell.swift
//  Weathery
//
//  Created by Macbook on 1/23/19.
//  Copyright © 2019 Spiritofthecore. All rights reserved.
//

import UIKit

class WeatherCell: UICollectionViewCell {
    
    @IBOutlet weak var TemperatureLabel: UILabel!
    @IBOutlet weak var CityLabel: UILabel!
    @IBOutlet weak var WeatherImage: UIImageView!
}
